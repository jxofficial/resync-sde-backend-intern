/**
 * Routes for account log in.
 */

const bcrypt = require('bcrypt');
const loginRouter = require('express').Router();
const Account = require('../models/account');


/**
 * Logs in with the username and password provided in request body.
 * Send 401 status code if username does not exist or password does not match password hash.
 */
loginRouter.post('/', async (req, resp) => {
  const body = req.body;
  const account = await Account.findOne({ username: body.username });
  const credentialsAreCorrect = account === null
    ? false
    : await bcrypt.compare(body.password, account.passwordHash);

  if (!credentialsAreCorrect) {
    return resp.status(401).json({ error: 'Invalid username or password' });
  } else {
    return resp.status(200).end();    
  }
});

module.exports = loginRouter;