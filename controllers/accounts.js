/**
 * Routes for account. Handles account creation.
 */

const accountsRouter = require('express').Router();
const bcrypt = require('bcrypt');
const Account = require('../models/account');

/**
 * Creates a new user account given a username and password in the POST request body.
 * Sends the created account object with userrname and password hash.
 * @throws exception if an error occurs when saving the newly created account to the database.
 */
accountsRouter.post('/createAccount', async (req, resp, next) => {
  const body = req.body;
  const SALT_ROUNDS = 10;
  const passwordHash = await bcrypt.hash(body.password, SALT_ROUNDS);

  const account = new Account({
    username: body.username,
    passwordHash
  });

  try {
    const createdAccount = await account.save();
    return resp.json(createdAccount.toJSON());
  } catch (exception) {
    next(exception);
  }
});

module.exports = accountsRouter;
