/**
 * Routes for employees.
 * Handles GET requests for single and all employees.
 * Handles employee profile creation.
 */

const employeesRouter = require('express').Router();
const Employee = require('../models/employee');

/**
 * Fetches all employees.
 * Sends JSON array containing all employees in the database.
 */
employeesRouter.get('/getEmployees', async (_req, resp) => {
  const documents = await Employee.find({});
  const parsedEmployees = documents.map((doc) => doc.toJSON());
  return resp.json(parsedEmployees);
});

/**
 * Fetches single employee given e_id in URL.
 * Sends a JSON object containing the employee matching the e_id in request params.
 * If employee is not found, status code 404 is sent.
 */
employeesRouter.get('/getEmployee/:e_id', async (req, resp) => {
  const id = req.params.e_id;
  const employee = await Employee.findOne({ employeeId: id });
  if (!employee) {
    return resp.status(404).json({ error: 'Employee not found' });
  }
  return resp.json(employee.toJSON());
});

/**
 * Creates an employee profile given a name and id in request body.
 * Sends the created profile object with status code 201.
 */
employeesRouter.post('/createEmployeeProfile', async (req, resp, next) => {
  const body = req.body;

  const employee = new Employee({
    name: body.name,
    employeeId: body.id,
    account: null
  });

  const savedEmployee = await employee.save();
  resp.status(201).json(savedEmployee.toJSON());
});

module.exports = employeesRouter;
