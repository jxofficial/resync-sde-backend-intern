/**
 * Express app object that contains all application middlewares and routing middlewares.
 */

const express = require('express');
const mongoose = require('mongoose');
const cors =  require('cors');

const employeesRouter = require('./controllers/employees');
const accountsRouter = require('./controllers/accounts');
const loginRouter = require('./controllers/login');

const config = require('./utils/config');
const logger = require('./utils/logger');

const app = express();

logger.info('Connecting to MongoDB ---');

mongoose
  .connect(config.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => logger.info('Connection successful'))
  .catch((err) => logger.error('Connection error', err.message));


/**
 * Uses middlewares that is required for CORS handling and parsing of request body.
 */
app.use(cors()); 
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/**
 * Route that handles reuqests to the homepage.
 */
app.get('/', (_req, res) => {
  res.send('Hello! Welcome to resync backend API.');
});

/**
 * Uses various routing middlewares.
 */
app.use('/', employeesRouter);
app.use('/', accountsRouter);
app.use('/login', loginRouter);

module.exports = app;