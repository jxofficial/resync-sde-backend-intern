/**
 * Handles server creation and listening of requests on the server.
 */

const app = require('./app')
const http = require('http');
const config = require('./utils/config');
const logger = require('./utils/logger');

const server = http.createServer(app);
server.listen(config.PORT, 
    () => logger.info(`Port ${config.PORT} on server is open and listening for API requests`));