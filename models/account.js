/**
 * Sets schema and model for Account.
 */
const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema({
  username: String,
  passwordHash: String
});

accountSchema.set('toJSON', {
  transform: (_document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
    delete returnedObject.passwordHash;
  }
});

const Account = mongoose.model('Account', accountSchema);

module.exports = Account;