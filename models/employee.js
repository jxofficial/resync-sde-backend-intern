/**
 * Sets schema and model for Employee.
 */

 const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
  name: String,
  employeeId: String,
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account'
  }
});

employeeSchema.set('toJSON', {
  transform: (_document, returnedObj) => {
    returnedObj.id = returnedObj._id.toString();
    delete returnedObj._id;
    delete returnedObj.__v;
    delete returnedObj.account;
  }
});

const Employee = mongoose.model('Employee', employeeSchema);

module.exports = Employee;
